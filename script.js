var puzzle = {};
var currentGame = { moveCount: 0,
				  	time: 0
				  };

function createBoard(){
	puzzle.listOfDivs = [];
	for(var i = 0; i < 4; i++){
		puzzle.listOfDivs[i] = []
	}
	for(var i = 0; i < 4; i++) {
		for(var j = 0; j < 4; j++) {
			if(i < 3 || j < 3){
				puzzle.listOfDivs[i][j] = document.createElement("div");
				puzzle.listOfDivs[i][j].id = "x" + i + "y" + j;
				puzzle.listOfDivs[i][j].className = "tile";
				puzzle.listOfDivs[i][j].style.top = (125 * j) + "px";
				puzzle.listOfDivs[i][j].style.left = (125 * i) + "px";
				puzzle.listOfDivs[i][j].style.backgroundPosition = (-125 * i) + "px " + (-125 * j) + "px";
				puzzle.gameBoard.appendChild(puzzle.listOfDivs[i][j]);
			}
			else{
				puzzle.listOfDivs[i][j] = document.createElement("div");
				puzzle.listOfDivs[i][j].id = "blank";
				puzzle.listOfDivs[i][j].className = "tile";
				puzzle.listOfDivs[i][j].style.top = (125 * j) + "px";
				puzzle.listOfDivs[i][j].style.left = (125 * i) + "px";
				puzzle.gameBoard.appendChild(puzzle.listOfDivs[i][j]);
				puzzle.blank = puzzle.listOfDivs[i][j];
			}
		}
	}
}

function createContainer(){
	puzzle.gameBoard = document.createElement("div");
	puzzle.gameBoard.addEventListener("click", moveEvent);
	puzzle.gameBoard.id="game-board";
	createBoard();
	
	puzzle.gameContainer.appendChild(puzzle.gameBoard);
	
	puzzle.showing = [];
	puzzle.information = makeExtension(0, "2bad77", "url('images/info.png')");
	fillInformation();
	puzzle.gameContainer.appendChild(puzzle.information);
	
	puzzle.language = makeExtension(1, "b26127", "url('images/lang.png')","50px");
	fillLanguage();
	puzzle.gameContainer.appendChild(puzzle.language);
	
	puzzle.loadMenu = makeExtension(2, "230da3", "url('images/save.png')" ,"100px");
	puzzle.gameContainer.appendChild(puzzle.loadMenu);
	fillLoadMenu();
}

function fillInformation(){
	var info = document.createElement("section");
	info.className = "extension-content";
	var title = document.createElement("h2");
	title.appendChild(document.createTextNode("INSTRUCTIONS"));
	title.id = "instruction-title";
	info.appendChild(title);	
	
	var ul = document.createElement("ul");
	var li = []
	
	for(var i = 0; i < 6; i++){
		li[i] = document.createElement("li");
		li[i].className = "information-list-item";
		ul.appendChild(li[i]);
	}
	
	li[0].appendChild(document.createTextNode("To start a game press start."));
	li[1].appendChild(document.createTextNode("To stop a game press stop."));
	li[2].appendChild(document.createTextNode("With the buttons on the left you can change the language, save your current game and load previous games."));
	li[3].appendChild(document.createTextNode("Press on a tile next to the empty one to move it."));
	li[4].appendChild(document.createTextNode("Once the image is back to its original state you win."));
	li[5].appendChild(document.createTextNode("Have fun and good luck!"));
	
	info.appendChild(ul);
	puzzle.information.appendChild(info);
}

function fillLanguage(){
	var langChoice = document.createElement("section");
	langChoice.className = "extension-content";
	var title = document.createElement("h2");
	title.appendChild(document.createTextNode("LANGUAGE"));
	title.id = "language-title";
	langChoice.appendChild(title);	
	
	var fButton = document.createElement("button");
	fButton.id = "french";
	fButton.appendChild(document.createTextNode("French"));
	fButton.addEventListener("click", function(){changeLanguage("french")});
	var eButton = document.createElement("button");
	eButton.id = "english"
	eButton.appendChild(document.createTextNode("English"));
	eButton.addEventListener("click", function(){changeLanguage("english")});
	
	langChoice.appendChild(fButton);
	langChoice.appendChild(eButton);
	
	puzzle.language.appendChild(langChoice);
}

function fillLoadMenu(){
	var loadMenu = document.createElement("section");
	loadMenu.className = "extension-content";
	var title = document.createElement("h2");
	title.appendChild(document.createTextNode("SAVE OR LOAD"));
	title.id = "load-menu-title";
	loadMenu.appendChild(title);
	var label = document.createElement("label");
	label.for = "name-of-game";
	label.appendChild(document.createTextNode("NAME OF SAVE"));
	
	var name = document.createElement("input");
	name.type = "text";
	name.id = "name-of-save";
	puzzle.nameOfGame = name;
	
	var sButton = document.createElement("button");
	sButton.id = "save";
	sButton.addEventListener("click", saveGame);
	sButton.appendChild(document.createTextNode("Save"));
	sButton.disabled = true;
	puzzle.saveButton = sButton;
	
	var lButton = document.createElement("button");
	lButton.id = "load";
	lButton.addEventListener("click", loadGame);
	lButton.appendChild(document.createTextNode("Load"));
	
	var selector = document.createElement("select");
	selector.id = "load-selector";
	var option = document.createElement("option");
	option.value = "";
	selector.appendChild(option);
	
	puzzle.loadSelector = selector;
	if(puzzle.hasLocalStorage){
		for(var i in localStorage){
			if(i.substr(0, 5) === "game:"){
				option = document.createElement("option")
				option.value = i;
				var text = document.createTextNode(i.substr(5, i.length - 5));
				option.appendChild(text);
				selector.appendChild(option);
			}
		}
	}
	
	loadMenu.appendChild(label);
	loadMenu.appendChild(name);
	loadMenu.appendChild(sButton);
	
	loadMenu.appendChild(document.createElement("hr"));
	
	loadMenu.appendChild(lButton);
	loadMenu.appendChild(selector);
	
	puzzle.loadMenu.appendChild(loadMenu);
	
}

function makeExtension(index, color, logo, top){
	puzzle.showing[index] = false;
	var extension = document.createElement("div");
	extension.className = "game-extension";
	var showHide = document.createElement("div");
	showHide.className = "show-hide";
	showHide.style.top = top;
	showHide.style.backgroundColor = "#"+color;
	showHide.style.backgroundImage = logo;
	showHide.appendChild(document.createTextNode(""));
	extension.appendChild(showHide);
	var body = document.createElement("div");
	body.className = "info";
	body.style.backgroundColor = "#"+color;
	if(top){
		body.style.borderTopLeftRadius = "10px";
	}
	extension.appendChild(body);
	showHide.addEventListener("click", function(){moveExtension(index, extension)});
	return extension;
}

function moveExtension(i, extension){
	 
	var direction;
	if(puzzle.showing[i]){
		direction = 0;
		setTimeout(function(){changeZIndexExtension(extension, -6)}, 400);
	}
	else{
		direction = -1
		extension.style.zIndex = 1;
		setTimeout(function(){changeZIndexExtension(extension, -4)}, 400);
	}

	extension.style.left = (direction * 300) + "px";
	puzzle.showing[i] = !puzzle.showing[i];
}

function changeZIndexExtension(extension, zIndex){
	extension.style.zIndex = zIndex;
}

function isAWin(){
	for(var i = 0; i < 4; i++) {
		for(var j = 0; j < 4; j++) {
			if(i < 3 || j < 3){
				if(puzzle.listOfDivs[i][j].id != "x" + i + "y" + j){
					return false;
				}
			}
		}
	}
	return true;
}

function move(elm){
	 
	if(puzzle.moving){
		return;
	}
	if(isBlankNextTo(elm)){
		puzzle.moving = true;
		setTimeout(setMovingToFalse, puzzle.timing);
		
		var blankX = (puzzle.blank.style.left.substr(0, puzzle.blank.style.left.length - 2) / 125);
		var blankY = (puzzle.blank.style.top.substr(0, puzzle.blank.style.top.length - 2) / 125);
		var clickedX = (elm.style.left.substr(0, elm.style.left.length - 2) / 125);
		var clickedY = (elm.style.top.substr(0, elm.style.top.length - 2) / 125);
		
		puzzle.listOfDivs[blankX][blankY] = elm;
		puzzle.listOfDivs[clickedX][clickedY] = puzzle.blank;
		
		//graphic movement
		
		var topOrLeft = blankX === clickedX ? "top" : "left";
		
		elm.style.transition =  topOrLeft + " ." + puzzle.timing + "s ease 0s";
		
		var tempTop = puzzle.blank.style.top;
		var tempLeft = puzzle.blank.style.left;
		
		puzzle.blank.style.top = elm.style.top;
		puzzle.blank.style.left = elm.style.left;
		
		elm.style.top = tempTop;
		elm.style.left = tempLeft;
		
		if(puzzle.playing){
			currentGame.moveCount++;
			updateMove();
			if(isAWin()){
				win();
			}
		}
	}
}

function win(){
	alert("you win!");
	if(puzzle.hasLocalStorage){
		if(localStorage.getItem("bmoves") == "--" || localStorage.getItem("bmoves") > currentGame.moveCount){
			localStorage.setItem("bmoves", currentGame.moveCount);
			puzzle.moveScore.textContent = currentGame.moveCount;
		}
		if(localStorage.getItem("btime") == "--"|| localStorage.getItem("btime") > currentGame.time){
			localStorage.setItem("btime", currentGame.time);
			puzzle.timeScore.textContent = currentGame.time + "s";
		}
		puzzle.imageSample.src = "";
	}
	
	stop();
}

function updateMove(){
	puzzle.moveCounter.textContent = currentGame.moveCount;
}

function updateTime(){
	currentGame.time++;
	puzzle.timeCounter.textContent = currentGame.time + "s";
}

function moveEvent(e){
	if(puzzle.playing){
		var clicked = e.target;
		var regex = /x[0-3]y[0-3]/;
		if(!regex.test(clicked.id)){
			return;
		}
		move(clicked);
	}
}

function moveRandom() {
	puzzle.moveRandomCounter++;
	
	if(puzzle.moveRandomCounter > 200){
		clearInterval(puzzle.scrambleTimer);
		moveToBottomRight();
		puzzle.timing="025";
		puzzle.moveRandomCounter = 0;
	}
	
	var blankX = (puzzle.blank.style.left.substr(0, puzzle.blank.style.left.length - 2) / 125);
	var blankY = (puzzle.blank.style.top.substr(0, puzzle.blank.style.top.length - 2) / 125);
	
	var coordXArray = [];
	var coordYArray = [];
	
	var counter = 0;
	
	if(blankX > 0 && !(blankX - 1 == puzzle.previousBlankX && blankY == puzzle.previousBlankY)){
		coordXArray[counter] = blankX - 1; 
		coordYArray[counter] = blankY;
		counter++;
	}
	
	if(blankX < 3 && !(blankX + 1 == puzzle.previousBlankX && blankY == puzzle.previousBlankY)){
		coordXArray[counter] = blankX + 1; 
		coordYArray[counter] = blankY;
		counter++;
	}
	
	if(blankY > 0 && !(blankX == puzzle.previousBlankX && blankY - 1 == puzzle.previousBlankY)){
		coordXArray[counter] = blankX;
		coordYArray[counter] = blankY - 1;
		counter++;
	}
	
	if(blankY < 3 && !(blankX == puzzle.previousBlankX && blankY + 1 == puzzle.previousBlankY)){
		coordXArray[counter] = blankX;
		coordYArray[counter] = blankY + 1;
	}
	
	do{
		var index = Math.floor((Math.random() * coordXArray.length));	
		var coordx = coordXArray[index];
		var coordy = coordYArray[index]
	} while(coordx == puzzle.previousBlankX && coordy == puzzle.previousBlankY);
	
	puzzle.previousBlankX = blankX;
	puzzle.previousBlankY = blankY;
	
	move(puzzle.listOfDivs[coordx][coordy]);
}

function scramble(){
	scrambleMessage();
	puzzle.moveRandomCounter = 0
	puzzle.timing = "025";
	puzzle.scrambleTimer = setInterval(moveRandom, 50);
}

function moveToBottomRight(){
	var i1 = setInterval(moveDown, 50);
	var i2;
	function moveDown(){	
		puzzle.timing = "025";
		var x = (puzzle.blank.style.left.substr(0, puzzle.blank.style.left.length - 2) / 125);
		var y = (puzzle.blank.style.top.substr(0, puzzle.blank.style.top.length - 2) / 125);
		
		if(y == 3){
			i2 = setInterval(moveRight, 50);
			clearInterval(i1);
			return;
		}
		
		move(puzzle.listOfDivs[x][y+1]);
	}
	function moveRight(){
		puzzle.timing = "025";
		var x = (puzzle.blank.style.left.substr(0, puzzle.blank.style.left.length - 2) / 125);
		var y = (puzzle.blank.style.top.substr(0, puzzle.blank.style.top.length - 2) / 125);
		
		if(x == 3){
			clearInterval(i2);
			puzzle.timing = "250";
			puzzle.gameContainer.removeChild(puzzle.scrambleScreen);
			puzzle.playing = true;
			puzzle.endButton.disabled = false;
			puzzle.timer = setInterval(updateTime, 1000);
			puzzle.saveButton.disabled = false;
			return;
		}
		move(puzzle.listOfDivs[x+1][y]);
	}
}

function setMovingToFalse(){
	puzzle.moving = false;
}

function isBlankNextTo(elm){
	var x = (elm.style.left.substr(0, elm.style.left.length - 2) / 125);
	var y = (elm.style.top.substr(0, elm.style.top.length - 2) / 125);
	
	if((x > 0 ? puzzle.listOfDivs[x-1][y].id === "blank" : false) ||
	   (x < 3 ? puzzle.listOfDivs[x+1][y].id === "blank" : false) ||
	   (y > 0 ? puzzle.listOfDivs[x][y-1].id === "blank" : false) ||
	   (y < 3 ? puzzle.listOfDivs[x][y+1].id === "blank" : false)) {
	   return true;
	}
	return false;
}

function start(){
	if(puzzle.categorySelector.value == ""){
		alert("You need to select a category.");
		return;
	}
	scramble();
	puzzle.startButton.disabled = true;
	puzzle.categorySelector.disabled = true;
    writeCookie(loadCookie() + 1 + ""); 
	puzzle.attemptsCounter.textContent = loadCookie();	
}

function stop(){
	puzzle.saveButton.disabled = true;
	
	puzzle.playing = false;
	puzzle.gameContainer.removeChild(puzzle.gameBoard);
	puzzle.gameBoard = document.createElement("div");
	puzzle.gameBoard.addEventListener("click", moveEvent);
	puzzle.gameBoard.id="game-board";
	createBoard();
	puzzle.gameContainer.appendChild(puzzle.gameBoard);
	
	clearInterval(puzzle.timer);
	currentGame.moveCount = 0;
	currentGame.time = 0;
	puzzle.timeCounter.textContent = "0s";
	puzzle.moveCounter.textContent = 0;	
	
	puzzle.startButton.disabled = false;
	puzzle.endButton.disabled = true;
	puzzle.categorySelector.disabled = false;
	puzzle.categorySelector.value = "";
	
	puzzle.imageSample.src="";
}

function scrambleMessage(){
	var backgroundDiv = document.createElement("div");
	backgroundDiv.id = "scrambleScreen";
	backgroundDiv.addEventListener("click", function(e){e.stopPropagation;}, true);
	
	var message = document.createElement("h2");
	message.appendChild(document.createTextNode(puzzle.isEnglish ? "Scrambling" : "Mélangement"));
	message.id = "scrambleMessage";
	
	backgroundDiv.appendChild(message);
	puzzle.scrambleScreen = backgroundDiv;
	puzzle.gameContainer.appendChild(backgroundDiv);
}

function changeLanguage(language){
	currentGame.language = language;
}

function changeImage(e){
	var category = e.target.value;
	var num = Math.floor((Math.random() * 5) + 1);
	var image = "images/" + category + "/" + num + ".jpg";
	currentGame.image = image;
	currentGame.category = category;
	if(category == ""){	
		image = "";
		puzzle.saveButton.disabled = true;
	} 
	puzzle.imageSample.src = image;
	for(var i = 0; i < 4; i++){
		for(var j = 0; j < 4; j++){
			if(i < 3 || j < 3){
				puzzle.listOfDivs[i][j].style.backgroundImage = "url('" + image + "')";
			}
		}
	}
}

function writeCookie(count){
    var expiry = new Date();
	expiry.setTime(expiry.getTime() + (7*24*60*60*1000));
	document.cookie = "attemptCount=" + count + ";expires=" + expiry.toGMTString() + ";";
}

function loadCookie(){
    var cookies = document.cookie.split(";");
    for(var i = 0; i < cookies.length; i++){
        var cookieList = cookies[i].split("=");
        if (cookieList[0] === "attemptCount"){
            var num = Number(cookieList[1]);
            return num;
        } 
    }
    writeCookie(0 + ""); 
	return 0;
}

function saveGame(){
	if(puzzle.hasLocalStorage){
		if(puzzle.nameOfGame.value == 0){
			alert("Please give a name to the game you wish to save");
		} else{
			var name = puzzle.nameOfGame.value;
			currentGame.gridPosition = [];
			for(var i = 0; i < 4; i++){
				currentGame.gridPosition[i] = []
			}
			for(var i = 0; i < 4; i++){
				for(var j = 0; j < 4; j++){
					currentGame.gridPosition[i][j] = puzzle.listOfDivs[i][j].id;
				}
			}
			localStorage.setItem("game:" + name, JSON.stringify(currentGame));
			alert("Current game saved succesfully with name: " + name);
			
			var option = document.createElement("option");
			option.value = "game:"+name;
			option.appendChild(document.createTextNode(name));
			puzzle.loadSelector.appendChild(option);
		}
	} else {
		alert("Your browser cannot save since it does not have localStorage available");
	}
	
}

function loadGame(){
	if(puzzle.loadSelector.value == ""){
		alert("you need to select a game to load");
	} else {
		currentGame = JSON.parse(localStorage.getItem(puzzle.loadSelector.value));
		puzzle.timeCounter.textContent = currentGame.time+"s";
		puzzle.moveCounter.textContent = currentGame.moveCount;
		puzzle.categorySelector.value = currentGame.category;
		puzzle.playing = true;
		puzzle.startButton.disabled = true;
		puzzle.imageSample.src = currentGame.image;
		for(var i = 0; i < 4; i++){
			for(var j = 0; j < 4; j++){
				var id = currentGame.gridPosition[i][j];
				var div = document.getElementById(id);
				puzzle.listOfDivs[i][j] = div;
				div.style.top = (125 * j) + "px";
				div.style.left = (125 * i) + "px";
				if(id !== "blank"){
					div.style.backgroundImage = "url('" + currentGame.image +"')";
				} else{
					puzzle.blank = div;
				}
			}
		}
	}
}

function preloadImages(){
	var r = new XMLHttpRequest();
	r.onreadystatechange = function(){
		if(r.readyState == 4 && r.status == 200){
			var images = r.responseText.split(",");
			var img = new Image();
			for(var i = 0; i < images.length; i++){
				img.src = images[i];
			}
		}
	}
	r.open("GET", "images/files.txt", true);
	r.send();
}

function changeLanguage(language){
	var r = new XMLHttpRequest();
	r.onreadystatechange = function(){
		if(r.readyState == 4 && r.status == 200){
			if(language == "english"){
				puzzle.isEnglish = true;
			} else{
				puzzle.isEnglish = false;
			}
			var words = r.responseText.split(";");
			puzzle.titleWindow.textContent = words[0];
			puzzle.title.textContent = words[0];
			puzzle.menuTitle.textContent = words[1];
			puzzle.categoryTitle.textContent = words[2];
			puzzle.animals.textContent = words[3];
			puzzle.cartoons.textContent = words[4];
			puzzle.games.textContent = words[5];
			puzzle.nature.textContent = words[6];
			puzzle.startStopTitle.textContent = words[7];
			puzzle.startButton.textContent = words[8];
			puzzle.endButton.textContent = words[9];
			puzzle.instructionTitle.textContent = words[10];
			puzzle.inst1.textContent = words[11];
			puzzle.inst2.textContent = words[12];
			puzzle.inst3.textContent = words[13];
			puzzle.inst4.textContent = words[14];
			puzzle.inst5.textContent = words[15];
			puzzle.inst6.textContent = words[16];
			puzzle.languageTitle.textContent = words[17];
			puzzle.frenchB.textContent = words[18];
			puzzle.englishB.textContent = words[19];
			puzzle.loadMenuTitle.textContent = words[20];
			puzzle.nameOfSave.textContent = words[21]
			puzzle.saveB.textContent = words[22];
			puzzle.loadB.textContent = words[23];
			puzzle.att1.textContent = words[24];
			puzzle.att2.textContent = words[25];
			puzzle.mText.textContent = words[26];
			puzzle.tText.textContent = words[27];
			puzzle.cMText.textContent = words[28];
			puzzle.cTText.textContent = words[29];
		}
	}
	r.open("GET", language+".txt", true);
	r.send();
}

function cacheLanguageInfo(){
	//necessary for language change
	puzzle.titleWindow = document.getElementsByTagName("title")[0];
	puzzle.title = document.getElementById("title");
	puzzle.menuTitle = document.getElementById("menu-title");
	puzzle.categoryTitle = document.getElementById("category-title");
	puzzle.animals = document.querySelector("option[value='animals']");
	puzzle.cartoons = document.querySelector("option[value='cartoons']");
	puzzle.games = document.querySelector("option[value='games']");
	puzzle.nature = document.querySelector("option[value='nature']");
	puzzle.startStopTitle = document.getElementById("start-stop-title");
	//puzzle.startButton
	//puzzle.endButton
	puzzle.instructionTitle = document.getElementById("instruction-title");
	puzzle.inst1 = document.querySelectorAll(".information-list-item")[0];
	puzzle.inst2 = document.querySelectorAll(".information-list-item")[1];
	puzzle.inst3 = document.querySelectorAll(".information-list-item")[2];
	puzzle.inst4 = document.querySelectorAll(".information-list-item")[3];
	puzzle.inst5 = document.querySelectorAll(".information-list-item")[4];
	puzzle.inst6 = document.querySelectorAll(".information-list-item")[5];
	puzzle.languageTitle = document.getElementById("language-title");
	puzzle.frenchB = document.getElementById("french");
	puzzle.englishB = document.getElementById("english")
	puzzle.loadMenuTitle = document.getElementById("load-menu-title");
	puzzle.nameOfSave = document.getElementById("name-of-save");
	puzzle.saveB = document.getElementById("save");
	puzzle.loadB = document.getElementById("load");
	puzzle.att1 = document.getElementById("att1");
	puzzle.att2 = document.getElementById("att2");
	puzzle.mText = document.getElementById("mText");
	puzzle.tText = document.getElementById("tText");
	puzzle.cMText = document.getElementById("cMText");
	puzzle.cTText = document.getElementById("cTText");
	puzzle.isEnglish = true;
}

function init(){
	preloadImages();
	puzzle.previousBlankX = 3;
	puzzle.previousBlankY = 3;
	puzzle.moving = false;
	puzzle.timing = 250;
	puzzle.gameContainer = document.getElementById("game-container");
	puzzle.startButton = document.getElementById("start");
	puzzle.startButton.addEventListener("click", start);
	puzzle.endButton = document.getElementById("stop");
	puzzle.endButton.addEventListener("click", stop);
	puzzle.categorySelector = document.getElementById("category-selector");
	puzzle.categorySelector.addEventListener("change", changeImage);
	puzzle.moveCounter = document.getElementById("cmoves");
	puzzle.timeCounter = document.getElementById("ctime");
	puzzle.imageSample = document.getElementById("image-sample");
	
	puzzle.moveScore = document.getElementById("bmoves");
	puzzle.timeScore = document.getElementById("btime");
	
	if(typeof(Storage) !== "undefined"){
		puzzle.hasLocalStorage = true;
	} else{
		puzzle.hasLocalStorage = false;
	}
	if(puzzle.hasLocalStorage){
		if(!localStorage.getItem("btime")){
			localStorage.setItem("btime", "--");
		}
		if(!localStorage.getItem("bmoves")){
			localStorage.setItem("bmoves", "--");
		}
		puzzle.moveScore.textContent = localStorage.getItem("bmoves");
		puzzle.timeScore.textContent = localStorage.getItem("btime") + "s";
	} else{
		puzzle.moveScore.textContent = "--";
		puzzle.timeScore.textContent = "--";
	}
	
	puzzle.attemptsCounter = document.getElementById("attempts");
	puzzle.attemptsCounter.textContent = loadCookie();
	
	createContainer();
	cacheLanguageInfo();	
}
window.addEventListener("DOMContentLoaded", init);